#!/bin/sh

#编译之前先安装golang 1.18.1版本已验证可用
#nodejs版本v16.20.0 npm 8.19.4版本验证可用，nodejs V18.16不行
#编译之前先 build ui:
# cd ui
# npm i
# npm run build 
# 开机启动方式： 
# 1. cp rttys.init /etc/init.d/rttys
#    update-rc.d rttys defaults 
#    /etc/init.d/rttys enable
#    /etc/init.d/rttys restart
# 2. cp ./rttys-linux-amd64/rttys /usr/local/bin/rttys
# 	mkdir -p /etc/rttys/
# 	cp rttys.conf /etc/rttys/
#    cp rttys.service /usr/lib/systemd/system/
#    systemctl enable rttys
#    systemctl restart rttys

#修改了默认不显示注册页面，手动通过/login?signup=1 地址进入注册页面
VersionPath="rttys/version"
GitCommit=$(git log --pretty=format:"%h" -1)
BuildTime=$(date +%FT%T%z)

[ $# -lt 2 ] && {
	echo "Usage: $0 linux amd64"
	exit 1
}

generate() {
	local os="$1"
	local arch="$2"
	local dir="rttys-$os-$arch"
	local bin="rttys"

	rm -rf $dir
	mkdir $dir
	cp rttys.conf $dir

	[ "$os" = "windows" ] && {
		bin="rttys.exe"
	}

	GOOS=$os GOARCH=$arch CGO_ENABLED=0 go build -ldflags="-s -w -X $VersionPath.gitCommit=$GitCommit -X $VersionPath.buildTime=$BuildTime" -o $dir/$bin
}

generate $1 $2
